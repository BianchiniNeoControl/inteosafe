//
//  LoginViewController.h
//  InteoSafe
//
//  Created by Ítalo Bianchini on 12/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginPresenter.h"

@interface LoginViewController : UIViewController

@property(nonatomic,strong) LoginPresenter *presenter;

@end
