//
//  LocalPersistenceBase.h
//  InteoSafe
//
//  Created by Neocontrol on 18/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Couchbaselite/CouchbaseLite.h>

@interface LocalPersistenceBase : NSObject

@property (nonatomic, strong) CBLDatabase *couchBase;
-(void)initCouchBase;

@end
