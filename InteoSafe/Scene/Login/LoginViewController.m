//
//  LoginViewController.m
//  InteoSafe
//
//  Created by Ítalo Bianchini on 12/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginConfigurator.h"
#import "DoLoginUseCase.h"
#include "DoLoginParameters.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmationEmailTextField;


@property (nonatomic,strong) LoginConfigurator *configurator;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self injectDependencies];
    
}

#pragma mark - Configurator

-(void)injectDependencies
{
    self.configurator = [[LoginConfigurator alloc]init];
    [self.configurator configure:self];
}

#pragma mark - Action Buttons

- (IBAction)buttonLoginTouch:(id)sender {
    [self doLogin];
}

#pragma mark - Do Login

-(void)doLogin
{
    if ([self validFields]) {
        [self.presenter addButtonPressed:[[DoLoginParameters alloc]initWitUser:self.nameTextField.text
                                                                      andPassword:self.emailTextField.text]];
    }else{
        [self invalidFields];
    }
}

#pragma mark - Validation Fields

-(BOOL)validFields
{
    return ![self.nameTextField.text isEqualToString:@""] && [self validEmail];
}

-(BOOL)validEmail
{
    return ![self.emailTextField.text isEqualToString:@""] && [self.emailTextField.text isEqualToString:self.confirmationEmailTextField.text];
}

-(void)invalidFields
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animation];
    animation.keyPath = @"position.x";
    animation.values =  @[ @0, @20, @-20, @10, @0];
    animation.keyTimes = @[@0, @(1 / 6.0), @(3 / 6.0), @(5 / 6.0), @1];
    animation.duration = 0.3;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    animation.additive = YES;
    
    if ([self.nameTextField.text isEqualToString:@""]) {
        [self.nameTextField.layer addAnimation:animation forKey:@"shake"];
    }
    
    if ([self.emailTextField.text isEqualToString:@""] || ![self.emailTextField.text isEqualToString:self.confirmationEmailTextField.text] ) {
        [self.emailTextField.layer addAnimation:animation forKey:@"shake"];
    }
    
    if ([self.confirmationEmailTextField.text isEqualToString:@""] || ![self.emailTextField.text isEqualToString:self.confirmationEmailTextField.text] ) {
        [self.confirmationEmailTextField.layer addAnimation:animation forKey:@"shake"];
    }
    
}


@end
