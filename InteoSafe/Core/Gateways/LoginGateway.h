//
//  LoginGateway.h
//  InteoSafe
//
//  Created by Ítalo Bianchini on 13/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import <Foundation/Foundation.h>
@class DoLoginParameters;


@protocol LoginGateway <NSObject>

-(void)doLogin:(DoLoginParameters *)parameters completion:(void (^)(BOOL flag))completion;

@end
