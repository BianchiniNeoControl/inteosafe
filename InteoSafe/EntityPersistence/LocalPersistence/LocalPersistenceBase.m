//
//  LocalPersistenceBase.m
//  InteoSafe
//
//  Created by Neocontrol on 18/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import "LocalPersistenceBase.h"

@interface LocalPersistenceBase ()

@end

@implementation LocalPersistenceBase

#pragma mark - CouchBase

-(void)initCouchBase
{
    NSError* error;
    self.couchBase = [[CBLManager sharedInstance] databaseNamed:[self dataBaseNamed] error: &error];
    if (!self.couchBase) {
        NSLog(@"Error -> %@",[error description]);
    }
    
    [self defineViewWithMapFunction];
    [self validationNamed];
}

-(NSString *)dataBaseNamed
{
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    bundleIdentifier = [bundleIdentifier stringByReplacingOccurrencesOfString:@"."
                                                                   withString:@""];
    
    return [bundleIdentifier lowercaseString];
}

-(void)defineViewWithMapFunction
{
    [[self.couchBase viewNamed: @"byDate"] setMapBlock: MAPBLOCK({
        id date = doc[@"created_at"];
        if (date)
            emit(date, doc);
    }) reduceBlock: nil version: @"1.1"];
    
    
}

//TODO: Pensar em nome melhor
-(void)validationNamed
{
    [self.couchBase setValidationNamed: @"created_at" asBlock: VALIDATIONBLOCK({
        if (newRevision.isDeletion)
            return;
        id date = (newRevision.properties)[@"created_at"];
        if (date && ! [CBLJSON dateWithJSONObject: date]) {
            [context rejectWithMessage: [@"invalid date " stringByAppendingString: [date description]]];
        }
    })];
}

@end
