//
//  LoginRouter.m
//  InteoSafe
//
//  Created by Ítalo Bianchini on 12/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import "LoginRouter.h"
#import "LoginViewController.h"

@interface LoginRouter ()

@property (nonatomic,strong) LoginViewController *loginViewController;

@end

@implementation LoginRouter


- (instancetype)initWith:(LoginViewController *)loginViewController
{
    self = [super init];
    if (self) {
        self.loginViewController = loginViewController;
    }
    return self;
}

@end
