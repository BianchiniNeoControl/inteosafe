//
//  LoginConfigurator.m
//  InteoSafe
//
//  Created by Ítalo Bianchini on 12/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import "LoginConfigurator.h"
#import "LoginViewController.h"
#import "LoginRouter.h"
#import "LoginPresenter.h"
#import "DoLoginUseCase.h"
#import "LocalPersistenceLoginGateway.h"
#import "CacheLoginGateway.h"
#import "LoginGateway.h"

@implementation LoginConfigurator

-(void)configure:(LoginViewController *)loginViewController
{
    LocalPersistenceLoginGateway *localPersistenceLoginGateway = [[LocalPersistenceLoginGateway alloc]init];
    
    //TODO: factory DAO
    //CacheLoginGateway *cache = [[CacheLoginGateway alloc] initWith:localPersistenceLoginGateway];
    
    DoLoginUseCase *dologinUseCase = [[DoLoginUseCase alloc]initWithGateway:localPersistenceLoginGateway];
    LoginRouter *loginRouter = [[LoginRouter alloc]initWith:loginViewController];
    loginViewController.presenter = [[LoginPresenter alloc]initWithView:loginViewController andDoLoginUseCase:dologinUseCase andRouter:loginRouter andDelegate:nil];
}


@end
