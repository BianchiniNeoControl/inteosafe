//
//  DoLoginUseCase.m
//  InteoSafe
//
//  Created by Ítalo Bianchini on 12/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import "DoLoginUseCase.h"
#import "DoLoginParameters.h"
#import "LoginGateway.h"

@interface DoLoginUseCase ()

@property id<DoLoginUseCase>doLoginUseCase;

@end

@implementation DoLoginUseCase


- (instancetype)initWithGateway:(id)loginGateway
{
    self = [super init];
    if (self) {
        self.doLoginUseCase = loginGateway;
    }
    return self;
}


-(void)doLogin:(DoLoginParameters *)parameters completion:(void (^)(BOOL flag))completion
{
    [self.doLoginUseCase doLogin:(DoLoginParameters *)parameters completion:^(BOOL flag) {
        if (flag) {
            completion(YES);
        }else{
            completion(NO);
        }
    }];
    
}

@end
