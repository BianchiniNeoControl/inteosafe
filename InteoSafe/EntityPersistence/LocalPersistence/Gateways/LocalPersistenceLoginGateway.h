//
//  LocalPersistenceLoginGateway.h
//  InteoSafe
//
//  Created by Ítalo Bianchini on 13/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LoginGateway.h"
#import "LocalPersistenceBase.h"

@interface LocalPersistenceLoginGateway : LocalPersistenceBase <LoginGateway>

- (instancetype)init;

@end
