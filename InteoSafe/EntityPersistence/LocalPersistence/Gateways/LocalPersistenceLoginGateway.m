//
//  LocalPersistenceLoginGateway.m
//  InteoSafe
//
//  Created by Ítalo Bianchini on 13/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import "LocalPersistenceLoginGateway.h"
#import "DoLoginParameters.h"
#import <Couchbaselite/CouchbaseLite.h>

@interface LocalPersistenceLoginGateway ()

@end

@implementation LocalPersistenceLoginGateway

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initCouchBase];
    }
    return self;
}

#pragma mark - Do Login

-(void)doLogin:(id)parameters completion:(void (^)(BOOL))completion
{
    NSError* error;
    if (![[self createEmptyDocument] putProperties: [self returnDocumentByDoLogin:(DoLoginParameters *)parameters] error: &error]) {
        //ERROR
        completion(NO);
    }else{
        completion(YES);
    }
}


#pragma mark - Tools

-(CBLDocument *)createEmptyDocument
{
    return [self.couchBase createDocument];
}

-(NSDictionary *)returnDocumentByDoLogin:(DoLoginParameters *)parameters
{
    return @{@"user":  parameters.user,
             @"password": parameters.password,
             @"check": @NO,
             @"created_at": [CBLJSON JSONObjectWithDate: [NSDate date]]};
}



@end
