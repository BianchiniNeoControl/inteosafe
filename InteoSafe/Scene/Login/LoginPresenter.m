//
//  LoginPresenter.m
//  InteoSafe
//
//  Created by Ítalo Bianchini on 12/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import "LoginPresenter.h"
#import "LoginViewController.h"
#import "DoLoginUseCase.h"
#import "LoginRouter.h"

@interface LoginPresenter () 

@property id <DoLoginView>doLoginView;
@property (nonatomic,strong) DoLoginUseCase *doLoginUserCase;
@property (nonatomic,strong) LoginRouter *loginRouter;

@end

@implementation LoginPresenter

- (instancetype)initWithView:(LoginViewController *)view andDoLoginUseCase:(DoLoginUseCase *)doLoginUseCase andRouter:(LoginRouter *)router andDelegate:(id)delegate
{
    self = [super init];
    if (self) {
        self.doLoginView = view; //TODO warning
        self.doLoginUserCase = doLoginUseCase;
        self.loginRouter = router;
        self.delegate = delegate;
    }
    
    return self;
}

#pragma mark - DoLoginPresenter Implementation

-(void)addButtonPressed:(DoLoginParameters *)parameters
{
    [self setupLayoutinitLogin];
    [self.doLoginUserCase doLogin:parameters completion:^(BOOL flag) {
        if (flag) {
            
        }else{
            
        }
    }];
}

-(void)setupLayoutinitLogin
{
    
}

#pragma mark - DoLoginView Implementation

-(void)updateLoginButtonState:(BOOL)enable
{

    
}


@end
