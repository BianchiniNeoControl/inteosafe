//
//  LoginRouter.h
//  InteoSafe
//
//  Created by Ítalo Bianchini on 12/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import <Foundation/Foundation.h>
@class LoginViewController;

@interface LoginRouter : NSObject

- (instancetype)initWith:(LoginViewController *)loginViewController;

@end
