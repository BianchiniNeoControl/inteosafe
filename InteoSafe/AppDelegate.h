//
//  AppDelegate.h
//  InteoSafe
//
//  Created by Ítalo Bianchini on 17/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

