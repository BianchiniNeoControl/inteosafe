//
//  DoLoginParameters.h
//  InteoSafe
//
//  Created by Ítalo Bianchini on 13/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DoLoginParameters : NSObject

@property (nonatomic,strong) NSString *user;
@property (nonatomic,strong) NSString *password;

- (instancetype)initWitUser:(NSString *)user andPassword:(NSString *)password;

@end
