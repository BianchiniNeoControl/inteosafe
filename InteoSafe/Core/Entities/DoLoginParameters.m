//
//  DoLoginParameters.m
//  InteoSafe
//
//  Created by Ítalo Bianchini on 13/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import "DoLoginParameters.h"

@implementation DoLoginParameters

- (instancetype)initWitUser:(NSString *)user andPassword:(NSString *)password
{
    self = [super init];
    if (self) {
        self.user = user;
        self.password = password;
    }
    return self;
}


@end
