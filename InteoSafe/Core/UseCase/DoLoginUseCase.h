//
//  DoLoginUseCase.h
//  InteoSafe
//
//  Created by Ítalo Bianchini on 12/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DoLoginParameters.h"

@protocol DoLoginUseCase <NSObject>

-(void)doLogin:(DoLoginParameters *)parameters completion:(void (^)(BOOL flag))completion;

@end

@interface DoLoginUseCase : NSObject <DoLoginUseCase>

- (instancetype)initWithGateway:(id)loginGateway;

@end
