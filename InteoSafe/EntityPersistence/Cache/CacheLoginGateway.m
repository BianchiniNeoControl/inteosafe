//
//  CacheLoginGateway.m
//  InteoSafe
//
//  Created by Ítalo Bianchini on 17/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import "CacheLoginGateway.h"
#import "LocalPersistenceLoginGateway.h"

@interface CacheLoginGateway ()

@property (nonatomic,strong) LocalPersistenceLoginGateway *localPersistenceLoginGateway;

@end

@implementation CacheLoginGateway

- (instancetype)initWith:(LocalPersistenceLoginGateway *)localPersistence
{
    self = [super init];
    if (self) {
        self.localPersistenceLoginGateway = localPersistence;
    }
    return self;
}





@end
