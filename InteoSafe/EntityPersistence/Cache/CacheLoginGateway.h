//
//  CacheLoginGateway.h
//  InteoSafe
//
//  Created by Ítalo Bianchini on 17/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import <Foundation/Foundation.h>
@class LocalPersistenceLoginGateway;

@interface CacheLoginGateway : NSObject

- (instancetype)initWith:(LocalPersistenceLoginGateway *)localPersistence;

@end
