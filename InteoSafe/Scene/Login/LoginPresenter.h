//
//  LoginPresenter.h
//  InteoSafe
//
//  Created by Ítalo Bianchini on 12/07/17.
//  Copyright © 2017 com.neocontrol.inteosafe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DoLoginParameters.h"
@class LoginViewController;
@class DoLoginUseCase;
@class LoginRouter;

@protocol LoginPresenterDelegate <NSObject>


@end


@protocol DoLoginPresenter <NSObject>

-(void)addButtonPressed:(DoLoginParameters *)parameters;

@end

@protocol DoLoginView <NSObject>

-(void)updateLoginButtonState:(BOOL)enable;

@end


@interface LoginPresenter : NSObject <DoLoginPresenter, DoLoginView>

@property id<LoginPresenterDelegate>delegate;

- (instancetype)initWithView:(LoginViewController *)view andDoLoginUseCase:(DoLoginUseCase *)doLogin andRouter:(LoginRouter *)router andDelegate:(id)delegate;

@end
